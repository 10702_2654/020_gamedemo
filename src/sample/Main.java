import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.shape.Rectangle;

import java.awt.*;

public class Main extends Application {
    @Override
    public void start(Stage stage) {

        Rectangle rectangle = new Rectangle();
        rectangle.setFill(Color.BLUE);

        //Setting the properties of the rectangle
        rectangle.setX(150.0f);
        rectangle.setY(75.0f);
        rectangle.setWidth(300.0f);
        rectangle.setHeight(150.0f);

        Group root = new Group(rectangle);
        Scene scene = new Scene(root, 500, 500);
        stage.setTitle("Game");
        stage.setScene(scene);
        stage.show();
    }
    public static void main(String args[]){
        launch(args);
    }
}